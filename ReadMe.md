# Landing Page

A simple landing page for a website made with only html and css. It follows the
following format and style specified by the open source course at: 
[The Odin Project](https://theodinproject.com)

## Desired Outcome
![desired outcome](./reference/outline.png)

## Colors and Fonts
![style](./reference/styles.png)
